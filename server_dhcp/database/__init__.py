import sqlite3

class HostDatabase(object):
    def __init__(self):
        conn = sqlite3.connect('knowHost.db')
        cursor = conn.cursor()
        cursor.execute(
            'CREATE TABLE IF NOT EXISTS KnowHost (\'mac\' TEXT, \'ip\' TEXT, \'hostname\' TEXT, \'last_used\' INTEGER)'
        )
        conn.commit()

    def get(self, **kw):
        conn = sqlite3.connect('knowHost.db')
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM KnowHost ' + self.create_where(**kw))
        return cursor.fetchall()
        #return list(map(Host.from_tuple, self.db.get(pattern)))

    def add(self, host):
        conn = sqlite3.connect('knowHost.db')
        cursor = conn.cursor()
        tupleHost = host.to_tuple()
        cursor.execute('INSERT INTO KnowHost VALUES (\''
                       + tupleHost[0] + '\',\''
                       + tupleHost[1] + '\',\''
                       + tupleHost[2] + '\','
                       + tupleHost[3] + ')')
        conn.commit()

    def delete(self, host = None, **kw):
        conn = sqlite3.connect('knowHost.db')
        cursor = conn.cursor()
        where = None
        if host:
            where = ' WHERE mac=\'' + host.mac + '\''
        else:
            where = ' ' + self.create_where(**kw)
        cursor.execute('DELETE FROM KnowHost' + where)
        conn.commit()

    def all(self):
        conn = sqlite3.connect('knowHost.db')
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM KnowHost')
        print(cursor.fetchall())

    def replace(self,host):
        self.delete(host)
        self.add(host)

    def update_last_used(self, host):
        conn = sqlite3.connect('knowHost.db')
        cursor = conn.cursor()
        where = None
        if host:
            where = ' WHERE mac=\'' + host.mac + '\''
        cursor.execute('UPDATE KnowHost SET last_used=' + str(host.last_used) + where)
        conn.commit()

    def create_where(self, **patern):
        result = "WHERE "
        is_pattern = False
        for element in patern:
            if element.__repr__() != 'ALL':
                result += element + '=\'' + str(patern[element]) + "\' AND "
                is_pattern = True
        if is_pattern:
            return result[:-4]
        else:
            return ""
